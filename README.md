### Introduction
- Created a simple django application which will return a JSON response while accessing the application with localhost:8000 or http://server_ip_address:8000
- We will be using Gitlab CI/CD platform to deploy this app to AWS EC2 instance
### Using the Application

Run locally:

```sh
$ docker-compose up -d --build
```

Verify [http://localhost:8000/](http://localhost:8000/) and it will show a Json response of: 

```json
{
  "Blog_Title": "Hello World!"
}
```


### AWS Setup
- Logged into EC2 Console from "AWS Console" Page

<img src="https://i.imgur.com/cV7uoRv.png"/>

- Selected "Amazon Linux 2 AMI" Machine image with "t2 Micro" to use aws service in free tier budget.

<img src="https://i.imgur.com/LeTYrny.png"/>

-  Configured the neccesary instance details before continuing to next page.

<img src="https://i.imgur.com/muVvaK8.png"/>

- Added a storage to use with the EC2 instance

<img src="https://i.imgur.com/hG1YRRr.png"/>

- Creating a new security group to allow traffic through port 80 and to access our instance with SSH on port 22 

<img src="https://i.imgur.com/rKodASu.png"/>

- Generated a key pair in AWS Console and stored it to our local computer's `.ssh` directory with proper permission


- Clicked on `View instance` to find our instance's public ip address
- Logged in to our ec2 instance with SSH from terminal by `ssh -i django-ec2-instance.pem ec2-user@54.236.28.228` 


- installed docker and docker-compose in our instance
- Then, we've generate a new SSH key with `ssh-keygen -t rsa` from within the instance.
- copied the generated public key to the `.ssh/authorized_keys` for passwordless SSH Login
- copied the content of the private key `cat ~/.ssh/id_rsa` for future use.

<img src="https://i.imgur.com/1uytJxI.png"/>

- Exit the remote SSH session. Set the key as an environment variable on your local machine and adding the key to the ssh-agent
- creating a new new directory for the app by using the newly created ssh access to our instance `ssh -o StrictHostKeyChecking=no ec2-user@54.236.28.228 mkdir /home/ec2-user/app `

<img src="https://i.imgur.com/Xpe57mO.png"/>

### Using Gitlab CI to deploy the app

- We have created a gitlab CI configuration file `.gitlab-ci-yml` to our project root directory and added the necessary configurations.
- We have created a bash script named `setup_env.sh` to generate the .env file for us. This file will create the required .env file, based on the environment variables found in our GitLab project's CI/CD settings.
- At this point we have created a project in gitlab and add the remote url to our local project directory and pushed our codebase to gitlab's master branch of our repository.
- Gitlab got triggered by getting updates on 'master' branch and start building our project


<img src="https://i.imgur.com/hqVvqHE.png"/>


- The build has been finished successfully and start deploying our project to EC2 instance

<img src="https://i.imgur.com/Jsz9oXO.png"/>

- As the build has been successfully passed, we can see the images in the GitLab Container Registry

<img src="https://i.imgur.com/pugz2K3.png"/>


<img src="https://i.imgur.com/BRqIFni.png"/>


- The deploy script has been failed at the very last moment

<img src="https://i.imgur.com/oUlzsgP.png"/>

- From the above screenshot, you will see the the runner has successfully logged in to our ec2 instance and failed to find Docker in our server
- After Digging the issue, found out that this issue is very common in gitlab Community edition and people faced this issue in time to time and and reported in last 5 days. (https://gitlab.com/gitlab-org/charts/gitlab/-/issues/478#note_407002408) . If I could get one/two days more, this issue will be fixed.

- Logging into our Ec2 instance, we can see the `.env` and `docker-compose.prod.yml` files are already in the server

<img src="https://i.imgur.com/ruIub8Q.png"/>

- What it will do next is 

```
  $ docker pull $IMAGE:web
  $ docker pull $IMAGE:nginx
  $ docker-compose -f docker-compose.prod.yml up -d
``` 
- After this three steps we could see our blog deployed in AWS EC2 instance on 54.236.28.228:8000 integrated in a complete automated CI/CD pipeline.
